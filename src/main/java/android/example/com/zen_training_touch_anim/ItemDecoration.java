package android.example.com.zen_training_touch_anim;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Lev on 20.01.2016.
 */
public class ItemDecoration extends RecyclerView.ItemDecoration {
    private static final int[] ATTRS = new int[]{
            android.R.attr.listDivider
    };
    private Drawable mDivider;

    public ItemDecoration(Context context) {
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        a.recycle();
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent) {
        final int childCount = parent.getChildCount();
        final int left = parent.getPaddingLeft();
        final int right = parent.getWidth() - parent.getPaddingRight();

        RecyclerViewAdapter adapter = (RecyclerViewAdapter) parent.getAdapter();
        RecyclerViewActivity.DataItem[] data = adapter.getData();
        for(int i=0; i<childCount; i++) {
            View child = parent.getChildAt(i);
            final int top = child.getTop();
            final int bottom = top + mDivider.getIntrinsicHeight();
            int childId = parent.getChildPosition(child);
            mDivider.setBounds(left, top, right, bottom);
            if(adapter.getSeparatorsIds().contains(childId)) {
                Rect rect = new Rect(left, top, right, top + 20);
                Paint pt = new Paint();
                String type = data[parent.getChildPosition(child)].type;
                if (type.equals(RecyclerViewActivity.DataItem.TYPE_TYPE1)) {
                    pt.setColor(Color.BLUE);
                } else {
                    pt.setColor(Color.RED);
                }
                c.drawRect(rect, pt);
            }
            mDivider.draw(c);
        }
    }
}
