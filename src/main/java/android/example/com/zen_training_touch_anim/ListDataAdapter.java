package android.example.com.zen_training_touch_anim;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by Lev on 17.01.2016.
 */
public class ListDataAdapter extends BaseAdapter {
    private Context mContext;
    private String[] mItems;

    public ListDataAdapter(Context context, String[] items) {
        mItems = items;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mItems.length;
    }

    @Override
    public Object getItem(int position) {
        return mItems[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ListViewHolder viewHolder;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, parent,false);
            TextView textView = (TextView) convertView.findViewById(R.id.itemTextView);
            viewHolder = new ListViewHolder(textView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ListViewHolder) convertView.getTag();
        }
        viewHolder.mTextView.setText(mItems[position]);
        return convertView;
    }

    public static class ListViewHolder {
        public TextView mTextView;

        public ListViewHolder(TextView v) {
            mTextView = v;
        }
    }
}
