package android.example.com.zen_training_touch_anim;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Lev on 14.01.2016.
 */
public class ListViewGroup extends ViewGroup implements GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener{
    private Context mContext;
    private BaseAdapter mAdapter = null;
    private static MotionEvent mPreviousEvent;
    private boolean mIsScrolling;
    private GestureDetectorCompat  mGestureDetector;

    public ListViewGroup(Context context) {
        super(context);
        mContext = context;
        initGestureDetector();
    }

    public ListViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initGestureDetector();
    }

    public ListViewGroup(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        initGestureDetector();
    }

    @Override
    public boolean shouldDelayChildPressedState() {
        return true;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final int action = MotionEventCompat.getActionMasked(ev);
        Log.i("DEV_touch", String.valueOf(action));


        if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
            // Release the scroll.
            mIsScrolling = false;
            mPreviousEvent = ev;
            return false; // Do not intercept touch event, let the child handle it
        }

        switch (action) {
            case MotionEvent.ACTION_MOVE: {
                mIsScrolling = true;
                return true;
            }
        }

        // In general, we don't want to intercept touch events. They should be
        // handled by the child view.
        mPreviousEvent = ev;
        return false;
    }

    /*@Override
    public boolean onTouchEvent(MotionEvent ev) {
        if(mIsScrolling && ev.getAction() == MotionEvent.ACTION_MOVE) {
            float yPath = ev.getY() - mPreviousEvent.getY();
            Log.i("DEV_yPath", String.valueOf(ev.getY()));
            Log.i("DEV_yPath", String.valueOf(mPreviousEvent.getY()));
            Log.i("DEV_yPath", String.valueOf(yPath));
            this.setY(this.getY() + yPath);
            mPreviousEvent = ev;
        }
        return true;
    }*/

    public void initGestureDetector() {
        mGestureDetector = new GestureDetectorCompat(getContext(), this);
        mGestureDetector.setOnDoubleTapListener(this);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.mGestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int count = 0;
        if(getAdapter() != null) {
            count = getAdapter().getCount();
        }
        Log.i("DEV_child_count", String.valueOf(count));
        int maxHeight = 0;
        final int maxWidth = MeasureSpec.getSize(
                widthMeasureSpec) - getPaddingLeft() - getPaddingRight();

        for(int i=0; i<count; i++) {
            View child = getChildAt(i);
            measureChild(child, widthMeasureSpec, heightMeasureSpec);
            if(child.getVisibility() != GONE) {
                maxHeight += child.getMeasuredHeight();
            }
        }

        Log.i("DEV_onMeasure", "maxWeight: " + maxWidth + " maxHeight" + maxHeight);
        setMeasuredDimension(maxWidth, maxHeight);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int count = getChildCount();
        final int childLeft = this.getPaddingLeft();
        final int childTop = this.getPaddingTop();
        final int childRight = r - l - getPaddingRight();
        final int childBottom = b - t - this.getPaddingBottom();
        final int childWidth = childRight - childLeft;
        final int childHeight = childBottom - childTop;
        int curWidth, curHeight, curLeft, curTop;
        curLeft = childLeft;
        curTop = childTop;
        Log.i("DEV_count", String.valueOf(count));
        for(int i=0; i<count; i++) {
            final View child = getChildAt(i);
            if(child.getVisibility() != GONE) {

                curWidth = child.getMeasuredWidth();
                curHeight = child.getMeasuredHeight();

                child.layout(curLeft, curTop, curLeft + curWidth, curTop + curHeight);
                curTop += curHeight;
            }
        }
    }

    public void setAdapter(BaseAdapter mAdapter) {
        this.mAdapter = mAdapter;
        LayoutInflater inflater = (LayoutInflater)
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int count = mAdapter.getCount();
        for(int i=0; i<count; i++) {
            final View child = inflater.inflate(R.layout.list_item, this, false);
            final TextView textView = (TextView) child.findViewById(R.id.itemTextView);
            textView.setText((String) mAdapter.getItem(i));
            textView.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        Toast.makeText(
                                mContext, textView.getText(), Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
            });
            this.addView(child, i);
        }
    }

    public BaseAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        Log.i("DEV_scroll", String.valueOf(distanceY));
        Log.i("DEV_scroll_data", String.valueOf(this.getY() + distanceY));
        int offset;
        if(distanceY>0) {
            offset = -10;
        } else  {
            offset = 10;
        }
        this.offsetTopAndBottom(offset);
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return false;
    }
}
