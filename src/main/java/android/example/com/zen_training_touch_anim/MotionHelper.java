package android.example.com.zen_training_touch_anim;

import android.util.Log;

/**
 * Created by Lev on 13.01.2016.
 */
public class MotionHelper {
    final public static int TYPE_RANDOM = 0;
    final public static int TYPE_HORIZONTAL = 1;
    final public static int TYPE_VERTICAL = 2;

    public static int checkMotionType(float startX, float startY,
                                    float finishX, float finishY) {
        float deltaX;
        float deltaY;

        deltaX = Math.abs(finishX - startX);
        deltaY = Math.abs(finishY - startY);
        Log.i("DEV", "deltaX: " + deltaX + "  deltaY: " + deltaY);

        if(deltaY < 100.0 && deltaX > 200.0) {
            return TYPE_HORIZONTAL;
        }
        else if(deltaX < 100.0 && deltaY > 200.0) {
            return TYPE_VERTICAL;
        } else {
            return TYPE_RANDOM;
        }
    }
}
