package android.example.com.zen_training_touch_anim;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;


public class RecyclerViewActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycled_view);

        DataItem[] data = new DataItem[20];
        for(int i=0; i<data.length; i++) {
            data[i] = new DataItem();
            data[i].value = "item"+i;
            if(i<10) { data[i].type = DataItem.TYPE_TYPE1; }
            else { data[i].type = DataItem.TYPE_TYPE2; }
        }

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(data);
        recyclerView.addItemDecoration(
                new ItemDecoration(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_recycled_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class DataItem {
        final static public String TYPE_TYPE1 = "Type 1";
        final static public String TYPE_TYPE2 = "Type 2";
        final static public int TYPES_COUNT = 2;
        public String value;
        public String type;

    }
}
