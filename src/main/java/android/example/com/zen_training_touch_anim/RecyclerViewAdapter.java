package android.example.com.zen_training_touch_anim;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Lev on 20.01.2016.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private RecyclerViewActivity.DataItem[] mData;

    public RecyclerViewAdapter(RecyclerViewActivity.DataItem[] data) {
        mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);
        RecyclerViewAdapter.ViewHolder holder = new RecyclerViewAdapter.ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.textView.setText(mData[i].value);
    }

    @Override
    public int getItemCount() {
        return mData.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public  View customView;
        public  TextView textView;
        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.itemTextView);
            customView = itemView;
        }
    }

    public RecyclerViewActivity.DataItem[] getData() {
        return mData;
    }

    public ArrayList<Integer> getSeparatorsIds() {
        ArrayList<Integer> separators = new ArrayList<>();
        String type = null;
        for(int i=0; i<mData.length; i++) {
            if(!mData[i].type.equals(type)) {
                separators.add(i);
                type = mData[i].type;
            }
        }
        return separators;
    }
}
