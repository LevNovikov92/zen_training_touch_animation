package android.example.com.zen_training_touch_anim;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;


public class TouchActivity extends ActionBarActivity {
    private float mStartX = 0.0f;
    private float mFinishX = 0.0f;
    private float mStartY = 0.0f;
    private float mFinishY = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touch);

        FrameLayout touchSpace = (FrameLayout) findViewById(R.id.touchSpace);
        touchSpace.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                float pathLength = 0.0f;
                int motionType = 0;

                TextView touchDesc = (TextView) findViewById(R.id.touchDescView);
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        touchDesc.setText("Frame Action Down");
                        mStartX = event.getX();
                        mStartY = event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        touchDesc.setText("Frame Action Move");
                        break;
                    case MotionEvent.ACTION_UP:
                        mFinishX = event.getX();
                        mFinishY = event.getY();
                        pathLength = getPathLength(mStartX, mStartY, mFinishX, mFinishY);
                        Log.i("DEV", String.valueOf(pathLength));
                        motionType = MotionHelper.checkMotionType(
                                mStartX, mStartY, mFinishX, mFinishY);

                        if(motionType == MotionHelper.TYPE_HORIZONTAL) {
                            Animation animation = AnimationUtils.loadAnimation(
                                    getApplicationContext(), R.anim.right_motion);
                            v.startAnimation(animation);
                        } else if(motionType == MotionHelper.TYPE_VERTICAL) {
                            Animation animation = AnimationUtils.loadAnimation(
                                    getApplicationContext(), R.anim.top_motion);
                            v.startAnimation(animation);
                        }
                        break;
                }
                return true;
            }
        });

        Button frameButton = (Button) findViewById(R.id.frameButton);
        frameButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    Toast.makeText(
                            getApplicationContext(), "Button OnTouch", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
    }

    private float getPathLength(float startX, float startY, float finishX, float finishY) {
        float pathLength = 0.0f;
        float deltaX = 0.0f;
        float deltaY = 0.0f;

        deltaX = finishX - startX;
        deltaY = finishY - startY;
        pathLength = (float) Math.sqrt(Math.pow(deltaX,2) + Math.pow(deltaY,2));
        return pathLength;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_touch, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
